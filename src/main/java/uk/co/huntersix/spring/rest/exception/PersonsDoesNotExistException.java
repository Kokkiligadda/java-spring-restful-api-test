package uk.co.huntersix.spring.rest.exception;

public class PersonsDoesNotExistException extends RuntimeException{

    public PersonsDoesNotExistException(String message) {
        super(message);
    }
    public PersonsDoesNotExistException(){

    }

}
