package uk.co.huntersix.spring.rest.model;

import java.util.List;

public class Persons {

    private List<Person> persons = null;

    public Persons persons(List<Person> persons) {
        this.persons = persons;
        return this;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public void setPersons(List<Person> persons) {
        this.persons = persons;
    }

}
