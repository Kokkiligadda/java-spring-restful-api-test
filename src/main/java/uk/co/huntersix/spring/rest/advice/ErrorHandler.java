package uk.co.huntersix.spring.rest.advice;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import uk.co.huntersix.spring.rest.exception.PersonsAlreadyExistedException;
import uk.co.huntersix.spring.rest.exception.PersonsDoesNotExistException;

@ControllerAdvice
public class ErrorHandler extends ResponseEntityExceptionHandler {

    @ResponseStatus(HttpStatus.NO_CONTENT)
    @ExceptionHandler(PersonsDoesNotExistException.class)
    public void handlePersonNotFound() { }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(PersonsAlreadyExistedException.class)
    public void handlePersonAlreadyExisted() { }


}
