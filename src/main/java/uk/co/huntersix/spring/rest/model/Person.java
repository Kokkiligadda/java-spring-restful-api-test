package uk.co.huntersix.spring.rest.model;

import org.springframework.util.StringUtils;

import java.util.concurrent.atomic.AtomicLong;

public class Person {
    private final static AtomicLong counter = new AtomicLong();

    private Long id;
    private String firstName;
    private String lastName;

    private Person() {
        // empty
    }

    public Person(String firstName, String lastName) {
        this.id = counter.incrementAndGet();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    @Override
    public boolean equals(Object object){
        if (object == this) {
            return true;
        }
        if (!(object instanceof Person)) {
            return false;
        }
        Person otherPerson = (Person) object;
        return this.firstName.equals(otherPerson.getFirstName())
                && this.lastName.equals(otherPerson.getLastName());
    }

    @Override
    public int hashCode() {
        int hash = createHashcode(firstName);
        hash = hash + createHashcode(lastName);
        return hash;
    }

    private int createHashcode(String str){
        int hash = 31;
        if(!StringUtils.isEmpty(str)){
            for (byte stringCharByte : str.getBytes()) {
                hash  = hash + stringCharByte*31;
            }
        }
        return hash;
    }


}
