package uk.co.huntersix.spring.rest.exception;

public class PersonsAlreadyExistedException extends RuntimeException{

    public PersonsAlreadyExistedException(String message) {
        super(message);
    }
    public PersonsAlreadyExistedException(){

    }

}
