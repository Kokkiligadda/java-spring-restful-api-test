package uk.co.huntersix.spring.rest.referencedata;

import org.springframework.stereotype.Service;
import uk.co.huntersix.spring.rest.exception.PersonsAlreadyExistedException;
import uk.co.huntersix.spring.rest.exception.PersonsDoesNotExistException;
import uk.co.huntersix.spring.rest.model.Person;
import uk.co.huntersix.spring.rest.model.PersonRequest;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PersonDataService {

    public static final List<Person> PERSON_DATA = new ArrayList<>();

    @PostConstruct
    public void setUpReferenceData(){
        PERSON_DATA.add(new Person("Mary", "Smith"));
        PERSON_DATA.add(new Person("Brian", "Archer"));
        PERSON_DATA.add(new Person("Collin", "Brown"));
    }

    public Person findPerson(String lastName, String firstName) {
        return PERSON_DATA.stream()
                .filter(p -> p.getFirstName().equalsIgnoreCase(firstName)
                        && p.getLastName().equalsIgnoreCase(lastName))
                .findFirst()
                .orElseThrow(() -> new PersonsDoesNotExistException("person does not exist"));
    }

    public List<Person> findPersonsMatchingWithSurname(String lastName) {
        List<Person> persons = PERSON_DATA.stream()
                .filter(p -> p.getLastName().equalsIgnoreCase(lastName))
                .collect(Collectors.toList());
        if(persons.size() > 0){
            return persons;
        } else {
            throw new PersonsDoesNotExistException("person does not exist");
        }
    }

    public void createPerson(PersonRequest personRequest) {
        Person createPerson = new Person(personRequest.getFirstName(), personRequest.getLastName());
        boolean personExisted = PERSON_DATA.stream().anyMatch(person ->
                personRequest.getFirstName().equals(person.getFirstName())
                && personRequest.getLastName().equals(person.getLastName()));

        if(personExisted){
            throw new PersonsAlreadyExistedException("Person already existed");
        }

        PERSON_DATA.add(createPerson);
    }
}
