package uk.co.huntersix.spring.rest.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import uk.co.huntersix.spring.rest.model.Person;
import uk.co.huntersix.spring.rest.model.PersonRequest;
import uk.co.huntersix.spring.rest.model.Persons;
import uk.co.huntersix.spring.rest.referencedata.PersonDataService;

import java.util.List;

@RestController
public class PersonController {
    private final PersonDataService personDataService;

    public PersonController(final PersonDataService personDataService) {
        this.personDataService = personDataService;
    }

    @GetMapping("/persons/{lastName}/{firstName}")
    public Person person(@PathVariable(value="lastName") String lastName,
                         @PathVariable(value="firstName") String firstName) {
        return personDataService.findPerson(lastName, firstName);
    }

    @GetMapping("/persons/{lastName}")
    public ResponseEntity<Persons> findPersonsWithSurname(@PathVariable(value="lastName") String lastName) {
        List<Person> personsMatchingWithSurname = personDataService.findPersonsMatchingWithSurname(lastName);
        Persons persons = new Persons();
        persons.setPersons(personsMatchingWithSurname);
        return new ResponseEntity<Persons>(persons, HttpStatus.OK);
    }

    @PostMapping(path = "/persons", consumes = "application/json", produces = "application/json")
    public ResponseEntity<Void> createPerson(@RequestBody PersonRequest personRequest) {
        personDataService.createPerson(personRequest);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
}