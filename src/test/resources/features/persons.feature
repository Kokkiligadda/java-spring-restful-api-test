Feature: Person Data

  Scenario: Should return no content response code when requested person does not exist.
    Given I have firstName as "firstName" and lastName as "lastName" to find out person
    When I request to find a person
    Then service should return 204 response code

  Scenario: Should return all persons with given surname.
    Given I have surname as "Smith" to find out all persons
    When I request to find all persons with given surname
    Then service should return success message with all persons.

  Scenario: Should create Person.
    Given I have Person to create
    When I request to create a Person with payload - "fixtures/create_person.json"
    Then service should return success message after successful creation of Person.

  Scenario: Should receive bad request if person already existed.
    Given I have Person to create
    When I request to create a already existed Person with payload - "fixtures/existed_person.json"
    Then service should return bad request response.