package uk.co.huntersix.spring.rest.utils;

import org.springframework.core.io.ClassPathResource;
import org.springframework.http.HttpHeaders;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.nio.charset.Charset;

public class TestUtils {

    public static String getPayloadFromFile(String jsonFile) throws IOException {
        return StreamUtils
                .copyToString(new ClassPathResource(jsonFile).getInputStream(), Charset.defaultCharset());
    }

    public static HttpHeaders generateHttpHeaders() {
        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json");
        httpHeaders.add("Accept", "application/json");
        return httpHeaders;
    }

}
