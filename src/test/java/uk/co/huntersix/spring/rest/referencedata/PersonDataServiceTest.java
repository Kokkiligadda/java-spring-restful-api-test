package uk.co.huntersix.spring.rest.referencedata;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import uk.co.huntersix.spring.rest.exception.PersonsAlreadyExistedException;
import uk.co.huntersix.spring.rest.exception.PersonsDoesNotExistException;
import uk.co.huntersix.spring.rest.model.Person;
import uk.co.huntersix.spring.rest.model.PersonRequest;

import java.util.List;

public class PersonDataServiceTest {

    private PersonDataService personDataService;

    @Before
    public void setUp(){
        personDataService = new PersonDataService();
    }

    @Test
    public void shouldReturnRequestedPersons(){
        String requestedFirstName = "Mary";
        String requestedLastName = "Smith";
        Person person = personDataService.findPerson(requestedLastName, requestedFirstName);
        Assert.assertEquals(requestedLastName, person.getLastName());
        Assert.assertEquals(requestedFirstName, person.getFirstName());
    }

    @Test(expected = PersonsDoesNotExistException.class)
    public void shouldThrowPersonsDoesNotExistExceptionWhenRequestedPersonIsNotAvailable(){
        String requestedFirstName = "NoName";
        String requestedLastName = "NoName";
        personDataService.findPerson(requestedLastName, requestedFirstName);
    }

    @Test(expected = PersonsDoesNotExistException.class)
    public void shouldThrowPersonsDoesNotExistExceptionWhenRequestedPersonsNotAvailableWithGivenSurname(){
        String requestedSurName = "NoName";
        personDataService.findPersonsMatchingWithSurname(requestedSurName);
    }

    @Test
    public void shouldReturnPersonMatchingWithGivenSurname(){
        String requestedSurName = "Archer";
        List<Person> personsMatchingWithSurname = personDataService.findPersonsMatchingWithSurname(requestedSurName);
        Assert.assertEquals("Brian", personsMatchingWithSurname.get(0).getFirstName());
        Assert.assertEquals("Archer", personsMatchingWithSurname.get(0).getLastName());
    }

    @Test(expected = PersonsAlreadyExistedException.class)
    public void shouldThrowExceptionWhenTryingToCreateSamePerson(){
        PersonRequest personRequest = new PersonRequest("Alan", "Turing");

        personDataService.createPerson(personRequest);

        personDataService.createPerson(personRequest);

    }

}