package uk.co.huntersix.spring.rest.acceptance.steps;

import io.cucumber.java.BeforeAll;
import io.cucumber.java8.En;
import io.restassured.RestAssured;
import org.springframework.boot.web.server.LocalServerPort;

import javax.annotation.PostConstruct;

public class CucumberRequestHelper implements En {

    @LocalServerPort
    private int SERVER_PORT_NUMBER;

    @PostConstruct
    public void setUpApiEndPoint() {
        RestAssured.port = SERVER_PORT_NUMBER;
    }

}
