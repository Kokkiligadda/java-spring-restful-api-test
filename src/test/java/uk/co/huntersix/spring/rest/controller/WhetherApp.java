package http;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

public class WhetherApp {

    public static void main(String[] args) {

        WhetherApp whetherApp = new WhetherApp();

        System.out.println(whetherApp.getWhetherInformation("London"));
    }

    public String getWhetherInformation(String place) {

        Location location = getLocation(place);


        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.openweathermap.org/data/2.5/weather?lat="+location.getLat()+"&lon="+location.getLon()+"&appid=0e629c5d7424fd28d5e8d2404d2e5658"))
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println(response.body());
        return response.body();

    }

    private Location getLocation(String place) {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(URI.create("https://api.openweathermap.org/geo/1.0/direct?q="+place+"&appid=0e629c5d7424fd28d5e8d2404d2e5658"))
                .method("GET", HttpRequest.BodyPublishers.noBody())
                .build();
        HttpResponse<String> response = null;
        Location[] locations = null;
        try {
            response = HttpClient.newHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            ObjectMapper objectMapper = new ObjectMapper();
            locations = objectMapper.readValue(response.body(), Location[].class);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return locations[0];
    }
}
