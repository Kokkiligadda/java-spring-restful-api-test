package uk.co.huntersix.spring.rest.acceptance;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@CucumberOptions(features = {"classpath:features/"})
@RunWith(Cucumber.class)
public class AcceptanceCriteriaIT {
}

