package uk.co.huntersix.spring.rest.acceptance.steps;

import io.cucumber.java8.En;
import io.restassured.response.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import uk.co.huntersix.spring.rest.utils.TestUtils;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;

public class PersonSteps implements En {

    @Autowired
    private CucumberRequestHelper cucumberRequestHelper;

    private String firstName;
    private String lastName;
    private final static String CREATE_PERSON_URI = "/persons";
    private final static String FIND_SINGLE_PERSON_URI = "/persons/{lastName}/{firstName}";
    private final static String FIND_PERSONS_WITH_SURNAME_URI = "/persons/{lastName}";
    private Response response;

    public PersonSteps(){
        Given("I have firstName as {string} and lastName as {string} to find out person",
                (String firstName, String lastName) -> {
            this.firstName = firstName;
            this.lastName = lastName;
        });
        When("I request to find a person", () -> {
            response = given()
                    .when()
                    .get(FIND_SINGLE_PERSON_URI, lastName, firstName);

        });
        Then("service should return {int} response code", (Integer responseCode) -> {
            response.then()
                    .assertThat()
                    .statusCode(responseCode);
        });

        Given("I have surname as {string} to find out all persons", (String lastName) -> {
            this.lastName = lastName;
        });

        When("I request to find all persons with given surname", () -> {
            response = given()
                    .when()
                    .get(FIND_PERSONS_WITH_SURNAME_URI, lastName);
        });

        Then("service should return success message with all persons.", () -> {
            response.then()
                    .assertThat()
                    .statusCode(HttpStatus.OK.value()).log().everything()
                    .body("persons[0].firstName", equalTo("Mary"))
                    .body("persons[0].lastName", equalTo("Smith"));
            ;
        });


        Given("I have Person to create", () -> {
        });

        When("I request to create a Person with payload - {string}", (String fileLocation) -> {
            response = given().body(TestUtils.getPayloadFromFile(fileLocation))
                    .headers(TestUtils.generateHttpHeaders())
                    .when()
                    .post(CREATE_PERSON_URI);

        });

        When("I request to create a already existed Person with payload - {string}", (String fileLocation) -> {
            response = given().body(TestUtils.getPayloadFromFile(fileLocation))
                    .headers(TestUtils.generateHttpHeaders())
                    .when()
                    .post(CREATE_PERSON_URI);

        });

        Then("service should return success message after successful creation of Person.", () -> {
            response.then()
                    .assertThat()
                    .statusCode(201);
        });

        Then("service should return bad request response.", () -> {
            response.then()
                    .assertThat()
                    .statusCode(400);
        });



    }
}
